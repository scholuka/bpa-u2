### Getting Started

1. Run DemoApplication.java (or run `docker-compose up`) and wait for Spring Boot startup
2. Go to our model in the web modeler: https://modeler.cloud.camunda.io/diagrams/768c8615-6c05-4497-a8d7-71023a6cea4b--u2
3. Click the *Run* button
4. Select the *bpa-cluster* cluster
5. In the *Variables* textarea paste the following JSON: `{"some_variable" : "test123"}`
6. Click *Run*
7. Go back to the Java console, it should now log "some_variable is set to test123" and "Doing some task..."