package com.example.demo;

import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import io.camunda.zeebe.spring.client.annotation.Variable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


@SpringBootApplication
public class DemoApplication {
	Random random = new Random();
	static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	static ConsoleHandler handler = new ConsoleHandler();
	@Autowired
	private ZeebeClient client;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		logger.setLevel(Level.ALL);
		handler.setLevel(Level.ALL);
		logger.addHandler(handler);
	}

	//This worker is called by running the U2 model and starts the order to cash process by triggering its message start event
	@JobWorker(type = "do-some-task")
	public void doSomeTask(@Variable String some_variable) {
		client.newPublishMessageCommand()
				.messageName("Hallo wir wollen nen Termin machen")
				.correlationKey("somekey")
				.messageId("message 1")
				.send();
	}

	@JobWorker(type = "Hier ein Terminvorschlag")
	public void sendDateProposal(@Variable String some_variable) {
		logger.info("Sending date proposal...");
	}

	@JobWorker(type = "check-stock")
	public void checkStock(final ActivatedJob job, @Variable String some_variable) {
		logger.info("Checking stock availability...");
		boolean isInStock = random.nextBoolean();
		logger.info("Item is " + (isInStock ? "" : "not ") + "in stock.");
		client.newCompleteCommand(job.getKey())
			.variables("{\"isInStock\": " + isInStock + "}")
			.send();
	}

	@JobWorker(type = "create-final-invoice")
	public void createFinalInvoice(@Variable String amount) {
		logger.info("The amount due for the invoice is " + amount);
		logger.info("Creating final invoice...");

		try (Writer fileWriter = new FileWriter("invoice.txt", false)) {
			fileWriter.write("""
%s

Invoice

Dear customer,

the balance due for your order is %s. Please pay within 14 days. Thanks for your business. If you have any questions about your invoice, let us know.

Best,
Mark
""".formatted(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), amount));
		} catch (IOException e) {
			logger.info("Could not create final invoice: " + e);
		}
	}
}